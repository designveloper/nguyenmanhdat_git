#### 2. Install Git
* Configuring Git
    * System `git config --system`
    * User `git config --global`
    * Project `git config`
* Adding color `git config --global color.ui true`

#### 3. Getting started
* Simple commit process
    * `git init`
    * `git add .`
    * `git commit -m "..."`
* Git log options
    * `git log -n 10`
    * `git log --since=2018-1-1`
    * `git log --until=2018-1-1`
    * `git log --author="Dat"`
    * `git log --grep="Init"`

#### 5. Making changes to files
* `git diff <file>`
* View only staged changes `git diff --staged`
* Delete file `git rm <file>`
* Rename file `git mv <file1> <file2>`
* Move file `git mv <file> dir/file`

#### 6. Undoing changes
* Dicarding change `git checkout -- <file>`
* Unstage changes `git reset HEAD <file>`
* Amending commits `git commit --amend -m "..."`
* Revert to latest commit `git revert ... `
* `git reset`
    * `--soft` does not change staging index or working directory
    * `--mixed` does not change working directory, change staging index to match repository
    * `--hard` changes staging index and working directory to match repository
* Removing untracked files: `git clean`

#### 8. Ignoring files
* Negate expression with '!'
* What to ignore:
    * compiled source code
    * packages and compressed files
    * logs and databases
    * OS genereated files
    * user-uploaded assets
* [Gitignore templates](https://github.com/github/gitignore)
* Ignoring globally `git config --global core.excludesfile ~/.gitignore_global`
* Ignoring tracked files `git rm --caced file.txt`
* Tracking empty directories by creating .gitkeep file 

#### 9. Navigating the commit tree
* Exploring tree listings `git ls-tree master assests/`
* Getting more from commit log
    * `git log --since=2.weeks --until=3.days`
    * `git log 2907d12..acf8750 --oneline`
    * `git log -p c4b913e.. <file>`
    * `git log --stat --summary`
* Viewing commits
    * `git show 2907d12`
    * `git show --format=oneline HEAD~3`
* Comparing commits
    * `git diff 2907d12 <file>`
    * `git diff --stat --sumaary 2907d12..HEAD <file>`

#### 12. Stashing changes
* `git stash save "..."`
* `git stash list`
* `git stash show -p stash@{0}`
* `git stash pop stash@{0}` or `git stash apply stash@{0}`
* `git stash drop stash@{0}`
* `git stash clear`

#### 13. Remote
* `git remote add <alias> <url>`
* `git remote rm <alias>`
* `git branch --set-upstream non_track origin/non_track`
* `git branch non_tracking origin/non_tracking`
* `git push origin --delete non_tracking`

#### 14. Tools
* `git config --global alias.co checkout`
* `git config --global alias.logg "log --graph --decorate --oneline --abbrev-commit --all"